# Creation VM Instance for app deployment

resource "google_compute_instance" "vm-instance" {
  name         = var.name
  machine_type = var.machine_type
  zone         = var.zone  # Update with your desired zone
  tags = var.networking_tags   # networking-tag

 boot_disk {
    initialize_params {
      image = var.boot_disk_image
    }
  }

  network_interface {
    subnetwork = var.subnetwork
    
  }

  depends_on = [var.depends-on]
  
  lifecycle {
    prevent_destroy = true
     
    #  ignore_changes = [tags]

     }   
   
}
