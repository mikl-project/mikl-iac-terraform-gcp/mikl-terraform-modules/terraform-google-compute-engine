
variable "name" {
    type = string
}

variable "machine_type" {
    type = string
}

variable "zone" {
    type = string
}

variable "networking_tags" {
    type = list(string)
}

variable "boot_disk_image" {
    type = string
}

variable "subnetwork" {
    type = string
}

variable "depends_on" {
    
}

